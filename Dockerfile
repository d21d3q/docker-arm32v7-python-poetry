ARG PYTHON_VERSION=latest

FROM arm32v7/python:${PYTHON_VERSION}

# install rust required by cryptography build
RUN apt-get update \
    && curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y  \
	&& . $HOME/.cargo/env	\
	&& pip install --upgrade pip setuptools	\
	&& pip install poetry
