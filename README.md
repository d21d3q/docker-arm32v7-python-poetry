# docker-arm32v7-python-poetry

Official [arm32v7/python](https://hub.docker.com/r/arm32v7/python/) image with preinstalled poetry

**docker hub**: [d21d3q/arm32v7-python-poetry](https://hub.docker.com/r/d21d3q/arm32v7-python-poetry)

Motivation behind this image is to have image with preinstalled poetry to save
few seconds on CI.

CI for this repository is scheduled to rebuild images monthly
in order to install latest versions of poetry.

Python versions:
- `latest`
- `3.10`
- `3.9`
- `3.8`
- `3.7`

Packages installed (latest):
- `poetry`
- `pip`
- `setuptools`

Additionaly `rust` is being installed which is required for building cryptography.
